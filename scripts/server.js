#!/usr/bin/env node

var fs = require('fs');
var express = require('express');
var server = express.createServer();
var config = JSON.parse(fs.readFileSync('package.json')).config;

server.use('/javascripts', express.static('build/web/javascripts'));
server.use('/stylesheets', express.static('build/web/stylesheets'));
server.use('/images', express.static('assets/images'));
server.use('/stylesheets/images', express.static('vendor/stylesheets/images'));
server.use('/assets', express.static('assets'));
server.use('/src', express.static('src'));
server.use('/vendor', express.static('vendor'));

server.get('/', function(req, res) {
  fs.createReadStream('build/web/index.html').pipe(res);
});

server.listen(config.port);
console.log('Server listening on http://0.0.0.0:' + config.port);
