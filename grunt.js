var fs = require('fs');
var path = require('path');
var wrench = require('wrench');

module.exports = function(grunt) {
  grunt.loadNpmTasks('bbb');

  grunt.initConfig({
    pkg: '<json:package.json>',

    meta: {
      libs: {
        common: [
          'vendor/javascripts/json2-*.js',
          'vendor/javascripts/jquery-*.js',
          'vendor/javascripts/underscore-*.js',
          'vendor/javascripts/backbone-*.js',
          'vendor/javascripts/jquery.mobile.custom.js',
          'vendor/javascripts/jquery.mobile-*.js'
        ],
        web: [
        ],
        android: [
          'vendor/javascripts/cordova-*.js'
        ],
        ios: [
          'vendor/javascripts/cordova-*.js'
        ]
      },
      src: [
        'src/app_web.js',
        'src/models/*.js',
        'src/collections/*.js',
        'src/data.js',
        'src/**/*.js'
      ],
      stylesheets: ['assets/stylesheets/**/*.css'],
      vendoredStylesheets: ['vendor/stylesheets/**/*.css']
    },

    lint: {
      grunt: ['grunt.js'],
      scripts: ['scripts/*.js'],
      src: '<config:meta.src>'
    },

    watch: {
      grunt: {
        files: '<config:lint.grunt>',
        tasks: 'lint:grunt'
      },
      scripts: {
        files: '<config:lint.scripts>',
        tasks: 'lint:scripts'
      },
      src: {
        files: '<config:meta.src>',
        tasks: 'lint:src copy'
      },
      libs: {
        files: [
          '<config:meta.libs.common>',
          '<config:meta.libs.android>',
          '<config:meta.libs.ios>'
        ],
        tasks: 'copy'
      },
      ejs: {
        files: 'templates/**/*.ejs',
        tasks: 'ejs'
      },
      jst: {
        files: 'templates/**/*.html',
        tasks: 'jst'
      }
    },

    config: {
      android: {
        dest: 'build/android/javascripts/app_web_config.js',
        options: {
          nativeClient: true
        }
      },
      ios: {
        dest: 'build/ios/javascripts/app_web_config.js',
        options: {
          nativeClient: true
        }
      },
      web: {
        dest: 'build/web/javascripts/app_web_config.js',
        options: {
          nativeClient: false
        }
      }
    },

    ejs: {
      android: {
        src: 'templates/ejs/index.html.ejs',
        dest: 'build/android/index.html',
        locals: {
          src: '<config:meta.src>',
          libs: ['<config:meta.libs.common>', '<config:meta.libs.android>'],
          vendoredStylesheets: '<config:meta.vendoredStylesheets>',
          stylesheets: '<config:meta.stylesheets>'
        }
      },
      ios: {
        src: 'templates/ejs/index.html.ejs',
        dest: 'build/ios/index.html',
        locals: {
          src: '<config:meta.src>',
          libs: ['<config:meta.libs.common>', '<config:meta.libs.ios>'],
          vendoredStylesheets: '<config:meta.vendoredStylesheets>',
          stylesheets: '<config:meta.stylesheets>'
        }
      },
      web: {
        src: 'templates/ejs/index.html.ejs',
        dest: 'build/web/index.html',
        locals: {
          src: '<config:meta.src>',
          libs: ['<config:meta.libs.common>', '<config:meta.libs.web>'],
          vendoredStylesheets: '<config:meta.vendoredStylesheets>',
          stylesheets: '<config:meta.stylesheets>'
        }
      }
    },

    // TODO: make this a multi task.
    jst: {
      'build/android/javascripts/templates.js': ['templates/jst/**/*.html'],
      'build/ios/javascripts/templates.js': ['templates/jst/**/*.html'],
      'build/web/javascripts/templates.js': ['templates/jst/**/*.html']
    },

    jshint: {
      options: {
        boss: true,
        browser: true,
        curly: true,
        eqeqeq: true,
        eqnull: true,
        es5: true,
        expr: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        node: true,
        sub: true,
        undef: true
      },
      globals: {
        '$': true,
        '_': true,
        App: true,
        Backbone: true,
        JST: true,
        PhoneGap: true,
        cordova: true
      }
    },

    copy: {
      android: [{
        src: 'vendor/stylesheets/images/',
        dest: 'build/android/stylesheets/'
      }, {
        src: 'vendor/javascripts/*',
        dest: 'build/android/vendor/javascripts/'
      }, {
        src: 'vendor/stylesheets/*',
        dest: 'build/android/vendor/stylesheets/'
      }, {
        src: 'src/',
        dest: 'build/android/'
      }, {
        src: 'assets/stylesheets/*',
        dest: 'build/android/assets/stylesheets'
      }, {
        src: 'assets/images/*',
        dest: 'build/android/assets/images'
      }],

      ios: [{
        src: 'vendor/stylesheets/images/',
        dest: 'build/ios/stylesheets/'
      }, {
        src: 'vendor/javascripts/*',
        dest: 'build/ios/vendor/javascripts/'
      }, {
        src: 'vendor/stylesheets/*',
        dest: 'build/ios/vendor/stylesheets/'
      }, {
        src: 'src/',
        dest: 'build/ios/'
      }, {
        src: 'assets/stylesheets/*',
        dest: 'build/ios/assets/stylesheets'
      }, {
        src: 'assets/images/*',
        dest: 'build/ios/assets/images'
      }],
    },

    clean: ['build'],

    remove: {
      android: ['build/android'],
      ios: ['build/ios'],
      web: ['build/web']
    }
  });

  grunt.registerTask('build', 'clean lint jst ejs config copy');

  grunt.registerTask(
    'build:android',
    'remove:android lint jst ejs:android config:android copy:android'
  );

  grunt.registerTask(
    'build:ios',
    'remove:ios lint jst ejs:ios config:ios copy:ios'
  );

  grunt.registerTask(
    'build:web',
    'remove:web lint jst ejs:web config:web'
  );

  // TODO: improve this task's code.
  grunt.registerMultiTask('copy', 'Copy static assets', function() {
    var _ = grunt.utils._;

    _(this.data).each(function(entry) {
      var stat;
      try {
        stat = fs.lstatSync(entry.src);
      } catch(error) {}

      if (stat && stat.isDirectory()) {
        var dest = path.join(entry.dest, entry.src.replace(path.dirname(entry.src), ''));
        grunt.log.writeln('Copying "' + entry.src + ' -> ' + dest + '".');
        wrench.mkdirSyncRecursive(dest);
        wrench.copyDirSyncRecursive(entry.src, dest);
      } else {
        _(grunt.file.expand(entry.src)).each(function(src) {
          var dest = path.join(entry.dest, src.replace(path.dirname(src), ''));
          grunt.log.writeln('Copying "' + src + ' -> ' + dest + '".');

          if (fs.lstatSync(src).isFile()) {
            grunt.file.copy(src, dest);
          } else {
            wrench.mkdirSyncRecursive(dest);
            wrench.copyDirSyncRecursive(src, dest);
          }
        });
      }
    });
  });

  grunt.registerMultiTask('remove', 'Remove contents of given directory', function() {
    var _ = grunt.utils._;

    _(this.data).each(function(entry) {
      _(grunt.file.expand(entry)).each(function(path) {
        grunt.helper('clean', path);
      });
    });
  });

  grunt.registerMultiTask('config', 'Create configuration file', function() {
    var _ = grunt.utils._;
    var environment = process.env.NODE_ENV || 'development';
    var config = _({
      platform: this.target
    }).extend(this.data.options, require('./config/environments.js')[environment]);
    var content = 'window.appWebConfig = ' + JSON.stringify(config) + ';';
    grunt.file.write(this.file.dest, content);
    grunt.log.writeln('File "' + this.file.dest + '" created.');
  });

  grunt.registerMultiTask('ejs', 'Compile EJS templates', function() {
    var _ = grunt.utils._;
    var environment = process.env.NODE_ENV || 'development';
    var data = require('fs').readFileSync(this.data.src, 'utf-8');
    var locals = _(this.data.locals).reduce(function(memo, value, key, object) {
      object[key] = grunt.file.expand(value);
      _(memo).extend(object);
      return(memo);
    }, {});
    var config = _({
      environment: environment
    }).extend(require('./config/environments.js')[environment]);

    grunt.file.write(this.data.dest, require('ejs').render(data, {
      locals: _({ '_': _, App: { Config: config } }).extend(locals)
    }));

    grunt.log.writeln('File "' + this.data.dest + '" created.');
  });
};
