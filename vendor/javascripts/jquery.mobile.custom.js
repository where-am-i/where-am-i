// Let backbone.js handle routing.

$(document).bind('mobileinit', function(){
  $.extend($.mobile, {
    ajaxEnabled: false,
    hashListeningEnabled: false,
    pushStateEnabled: false,
    loadingMessage: 'Carregando...',
    loadingMessageTheme: 'b',
    loadingMessageTextVisible: true
  });
});
