App.RegionManager = {
  addRegions: function(regions, context) {
    var addRegion = function(value, key) {
      context[key] = new App.Region({ el: value });
    };
    _(regions).each(addRegion);
  }
};
