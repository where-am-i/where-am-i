App.Views.PlanetList = Backbone.View.extend({
  tagName: 'ul',
  attributes: {
    'data-role': 'listview'
  },
  template: JST['templates/jst/planet_list.html'],
  initialize: function() {
    _.bindAll(this);

    this.planetListItemViews = this.collection.map(function(model) {
      return(new App.Views.PlanetListItem({ model: model }));
    });
  },
  render: function() {
    this.$el.html(this.template());

    var that = this;
    _(this.planetListItemViews).each(function(view) {
      that.$el.append(view.render().el);
    });

    return(this);
  }
});
