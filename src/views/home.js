App.Views.Home = Backbone.View.extend({
  className: 'home',
  template: JST['templates/jst/home.html'],
  initialize: function() {
    _.bindAll(this);
  },
  render: function() {
    this.$el.html(this.template());
    return(this);
  }
});
