App.Views.ShowPlanet = Backbone.View.extend({
  template: JST['templates/jst/show_planet.html'],
  initialize: function() {
    _.bindAll(this);
  },
  render: function() {
    this.$el.html(this.template());
    return(this);
  }
});
