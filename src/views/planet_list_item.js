App.Views.PlanetListItem = Backbone.View.extend({
  tagName: 'li',
  template: JST['templates/jst/planet_list_item.html'],
  events: {
    'click': 'onClick'
  },
  initialize: function() {
    _.bindAll(this);
  },
  render: function() {
    this.$el.html(this.template({ model: this.model }));
    return(this);
  },
  onClick: function(event) {
    event.preventDefault();

    App.router.navigate(App.router.showPlanetPath(this.model.id), { trigger: true });
  }
});
