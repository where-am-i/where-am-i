App.Views.ShowPlanets = Backbone.View.extend({
  template: JST['templates/jst/show_planets.html'],
  initialize: function() {
    _.bindAll(this);

    this.planetListView = new App.Views.PlanetList({
      collection: this.collection
    });
  },
  render: function() {
    this.$el.html(this.template());
    this.$el.append(this.planetListView.render().el);
    return(this);
  }
});
