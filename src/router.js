App.Router = Backbone.Router.extend({
  routes: {
    ''              : 'index',
    'home'          : 'home',
    'planets'       : 'showPlanets',
    'planets/:name' : 'showPlanet'
  },
  indexPath       : function() { return(''); },
  homePath        : function() { return('home'); },
  showPlanetsPath : function() { return('planets'); },
  showPlanetPath  : function(name) { return('planets/' + name); },
  index: function() {
    this.navigate(this.homePath(), { trigger: true, replace: true });
  },
  home: function() {
    var homeView = new App.Views.Home();
    App.contentRegion.show(homeView);
  },
  showPlanets: function() {
    var view = new App.Views.ShowPlanets({
      collection: App.Data.planets
    });
    App.contentRegion.show(view);
  },
  showPlanet: function(name) {
    var view = new App.Views.ShowPlanet({
      model: App.Data.planets.get(name)
    });
    App.contentRegion.show(view);
  }
});
