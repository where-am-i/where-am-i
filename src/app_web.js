var App = {
  Models: {},
  Views: {},
  Collections: {},
  Config: window.appWebConfig,
  init: function() {
    $.ajaxSetup({
      // PhoneGap on android caches AJAX requests when they're not supposed to
      // be cached, so we force jQuery not to do it. See http://bit.ly/oDcsEM.
      cache: App.Config.platform !== 'android'
    });

    App.RegionManager.addRegions({
      contentRegion: '#content-region'
    }, App);

    App.router = new App.Router();

    Backbone.history.start();
  }
};
