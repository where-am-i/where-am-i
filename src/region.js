App.Region = Backbone.View.extend({
  show: function(view, options) {
    options || (options = {});
    options.render || (options.render = true);

    if (options.render) {
      view.render();
    }

    this.$el.html(view.el).show();
    this.$el.trigger('create');
  },
  hide: function() {
    this.$el.hide();
  }
});
